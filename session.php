<?php

	session_start();
	
	require_once 'class.user.php';
	$session = new USER();
	
	// if user session is not active(not loggedin) this page will help pages to redirect to login page
	
	if(!$session->is_loggedin())
	{
		// session no set redirects to login page
		$session->redirect('index.php');
	}
